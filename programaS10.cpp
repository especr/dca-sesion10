// Sesión 10 - DCA 
// Esperanza Cutillas Rastoll
// DNI: 48721162D

#include <iostream>
#include <list>
#include <string>
using namespace std;

// El programa está pensado para que el programa sea como una lista de la compra a la que se le podrán hacer consultas.
const string no_func="Funcionalidad aún no disponible. Por favor seleccione otra opción.";
const string mensajeErrorMenu="La opción que has elegido es incorrecta. Elija una opción de las disponibles.\n";
const string pedir_add_nombre_elemento="Escribe el nombre del elemento que vas a añadir a la lista: ";
const string done_add_elemento="Elemento añadido";
void mostrarMenu()
{
	cout << "1. Añadir elemento a la lista" << endl;
	cout << "2. Borrar elemento de la lista" << endl;
	cout << "3. Mostrar lista" << endl;
	cout << "4. Salir" << endl;
}

void opcion1(list<string> &lista)
{
	string nombre;
	cout << pedir_add_nombre_elemento <<endl;
	cin >> nombre;
	lista.push_back(nombre);
	cout << done_add_elemento << endl;
}

void opcion3(list<string> &lista) // Este método se encarga de mostrar la lista de elementos
{
	std::list<string>::iterator pos;
	pos = lista.begin(); // Pos estará en el inicio de la lista
	int i=1;
	while(pos != lista.end())
	{
		cout <<i;
		cout <<": ";
		cout <<*pos;	// Vamos mostrando cada uno de los elementos de la lista
		cout << endl;
		pos++;
		i++;
	}
	cout << "---------------\n" << endl;
}

int main()
{
	list<string> listaDeElementos;	// Lista de elementos usando las librerías list y string
	cout << "Hola, con este programa puedes gestionar una lista de items." << endl;
	int op=0;
	do 
	{
		mostrarMenu();
		cin>>op;
		switch(op)
		{
			case 1:
				opcion1(listaDeElementos);
				break;
			case 2:
				cout << no_func << endl; // función aún no implementada
				break;
			case 3:
				opcion3(listaDeElementos);
				break;
			case 4:
				cout << "Gracias por usar el programa" << endl;
				break;
			default:
				cout << mensajeErrorMenu << endl;
				break;
		}
	} while(op!=4);
	return 0;
}

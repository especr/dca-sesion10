// Esperanza Cutillas Rastoll
// DNI: 48721162D
// Práctica 10 de DCA

Enlace al repositorio: https://especr@bitbucket.org/especr/dca-sesion10.git

Comandos utilizados:

mkdir proyecto-dca-s10
cd proyecto-dca-s10/
git init 
git remote add origin https://especr@bitbucket.org/especr/dca-sesion10.git

Después de eso, al intentar hacer un commit, después de haber añadido este fichero con git add Readme.txt,
nos mostraba lo siguiente:


*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.


Ya que debemos configurar el usuario y el correo para el usuario. Así que escribimos, para realizar la configuración local:

[alu@arch-eps proyecto-dca-s10]$ git config user.email "ecr39@alu.ua.es"
[alu@arch-eps proyecto-dca-s10]$ git config user.name "Esperanza Cutillas Rastoll"

Entonces ya podemos hacer el commit con:
[alu@arch-eps proyecto-dca-s10]$ git commit -m "Escribiendo el readme"
[master (root-commit) 6fd3f28] Escribiendo el readme
 1 file changed, 26 insertions(+)
 create mode 100644 Readme.txt

Además para ver qué cambios hay en la rama actual empleamos el comando:
git status

Si no hay cambios nos devuelve lo siguiente:
On branch master
nothing to commit, working tree clean

Después de haber hecho el commit es hora de subir los cambios con:
git push -u origin master

// El programa está pensado para que el programa sea como una lista de la compra a la que se le podrán hacer consultas.

Podemos ir haciendo commits y subiendo los cambios a origin hasta que queramos.
// -------------- Alias (locales y globales) --------------------------
Antes de nada, vamos a crear alias locales y globales, se hace de la misma forma sólo que en un caso es sin --global y en el otro con.
Por ejemplo para configurar co como alias para commit:
[alu@arch-eps proyecto-dca-s10]$ git config alias.co commit
En el caso de ser global sería: git config --global alias.co commit
// Y vemos que de hecho funciona porque al escribir git co:

[alu@arch-eps proyecto-dca-s10]$ git co
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
	modified:   Readme.txt

Untracked files:
	p

no changes added to commit
/ ------------------- alias para status -------------------
También para usar un alias de git status:
[alu@arch-eps proyecto-dca-s10]$ git config alias.st status
[alu@arch-eps proyecto-dca-s10]$ git st
En el caso de ser alias global es: git config --global alias.st status

// --------------- git hooks ---------------------
Con el comando git hooks --help obtendremos información sobre los hooks de git.
En mi caso voy a usar pre-commit, por lo que le quito la extensión sample. Se encuentra dentro de la carpeta .git/hooks/ .
Este "hook" es invocado por git commit, la utilidad por defecto de este "hook" es atrapar espacios en blanco al final de cada línea y abortar el "commit" si esto llega a ocurrir. Pero se puede agregar cualquier cantidad de pruebas a este "hook" para asegurar que nuestros "commits" se encuentran como queremos que estén. Se puede ignorar este "hook" utilizando la bandera --no-verfy al final del "commit".
Si hacemos un commit con los cambios actuales nos devuelve lo siguiente:
Readme.txt:61: new blank line at EOF.
programaS10.cpp:34: trailing whitespace.
+	while(pos != lista.end())
Después de quitar los espacios en blanco al final de fichero, donde nos indica git ya podemos hacer el commit.

// --------------- git bisect ---------------------
Después de añadir un fallo en la parte donde se llama a la opcion2() del programa hemos hecho un par de commits más para poder usar git bisect.
git bisect start
git bisect bad # Si el commit actual tiene errores
# Luego marcamos un commit que sepamos que funciona:
git bisect good f51bbf345a4e513b3d22efd7917ff7ab0d435748
Bisecting: 1 revision left to test after this (roughly 1 step)
[08b4a9248ae1be8025119318f2c2a576af6243c1] Mensajes guardados en constantes


Y luego vamos haciendo git bisect bad hasta que lleguemos a este punto:

[alu@arch-eps proyecto-dca-s10]$ git bisect bad
8c80b2704e236431bd489d0dc04e22bc0735ffd2 is the first bad commit
commit 8c80b2704e236431bd489d0dc04e22bc0735ffd2
Author: Esperanza Cutillas Rastoll <ecr39@alu.ua.es>
Date:   Tue Dec 27 12:19:01 2016 +0100

    Funcionalidades de mostrar lista y de añadir elemento añadida.

:100644 100644 4f00a7254bbaaf0124d37a5ebfb63023aee044fb 7cd0dfeee2ce330e280e4a547263f21b69454707 M	Readme.txt
:100644 100644 3a2d0fbb0b2d4a3c5e5bfb371cb57a5c6ac46e30 2166753d3eb63204c9ec08779dfdbd657945d784 M	programaS10.cpp
Compilación: g++ programaS10.cpp -o p